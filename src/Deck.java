public class Deck {

	private static String suit;
	private static String number;
	
	public static String getSuit() {
		return suit;
	}

	public static void setSuit(String suit) {
		Deck.suit = suit;
	}

	public static String getNumber() {
		return number;
	}

	public static void setNumber(String number) {
		Deck.number = number;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// make the cards
		int[] d = new int[52]; // Represents total number of cards in the deck.
		
		String[] suit = {"Hearts", "Diamonds", "Clubs", "Spades"};
		String[] number = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", 
						   "Jack", "Queen", "King"};
    
    
    	//Initializing the Cards from the deck.
		for (int i = 0; i < d.length; i++){
			d[i] += i;
		}
		
		//Shuffle cards from the deck.
		for (int i = 0; i < d.length; i++) {
			int ind = (int)(Math.random() * d.length);
			int x = d[i]; // deck has 52 cards
			d[i] = d[ind]; // picks a random card
			d[ind] = x; // picks another random card
			
		}
    	
		//Shows 4 cards
		for (int i = 0; i < 4; i++) {
			setSuit(suit[d[i] / 13]);
			setNumber(number[d[i] % 13]);
			System.out.println("You drew the " + number + " of " + suit);
		}
		
	}

	
	
}
	
    
